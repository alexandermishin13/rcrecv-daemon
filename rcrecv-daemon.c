/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (C) 2021 Alexander Mishin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/*
 * GPIORCRECV - Remote control receiver module over GPIO.
 *
 * Remote control receiver module can't be discovered automatically, please
 * specify hints as part of loader or kernel configuration:
 *	hint.rcrecv.0.at="gpiobus0"
 *	hint.rcrecv.0.pins=<PIN>
 *
 * Or configure via FDT data.
 */

#include <sys/event.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <stdbool.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <syslog.h>
#include <paths.h>

//#include <sys/wait.h>

#include <libutil.h>
#include <libgpio.h>

#include <dev/rcrecv/rcrecv.h>

#define DEVICE_GPIOC  "/dev/gpioc0"
#define DEVICE_RCRECV "/dev/rcrecv"
#define MAX_FILENAME 255
#define MAX_CMDARGS_NUMBER 4

static const char *dev_rcrecv = DEVICE_RCRECV;
static const char *dev_gpio = DEVICE_GPIOC;
static int dev;
static gpio_handle_t gpioc;
static struct pidfh *pfh = NULL;
static unsigned long interval = 1000; // Minimal interval between codes is 1s
static char *pidfile = NULL;

static bool background = false;
static bool args_limit = false;
static bool be_verbose = false;

static struct option long_options[] = {
    {"help",     no_argument,       NULL, 'h' },
#define PIN_SET 1
    {"set",      required_argument, NULL, 's' },
#define PIN_UNSET 2
    {"unset",    required_argument, NULL, 'u' },
#define PIN_TOGGLE 3
    {"toggle",   required_argument, NULL, 't' },
#define CMD_EXECUTE 4
    {"execute",  required_argument, NULL, 'e' },
    {"device",   required_argument, NULL, 'd' },
    {"gpio",     required_argument, NULL, 'g' },
    {"interval", required_argument, NULL, 'i' },
    {"verbose",  no_argument,       NULL, 'v' },
    {NULL,       0,                 NULL,  0  }
};

typedef struct rcc_entry {
    uint32_t	 code;
    gpio_pin_t	 pin;
    int		 action;
    char	*argv[MAX_CMDARGS_NUMBER + 2];

    SLIST_ENTRY(rcc_entry) rcc_entries;
} *rcc_entry_t;

SLIST_HEAD(rcc_listhead, rcc_entry) rcc_head = SLIST_HEAD_INITIALIZER(rcc_head);

/*
 * Short help
 */
static void
usage(void)
{
    fprintf(stderr,
"Usage: %s [options]\n", getprogname());
    fprintf(stderr,
"Where options are:\n"
"	-d, --device=<ctldev>\n"
"			A remote control receiver device name.\n"
"			Default: /dev/rcrecv\n"
"	-g, --gpio=<gpioc>\n"
"			A gpio controller device name. Default: /dev/gpioc0\n"
"	-i, --interval=<ms>\n"
"			A minimal valid interval between repeated codes. If\n"
"			an interval is less than that value the next same ones\n"
"			will be ignored. Default value is 1000ms\n"
"	-s, --set code=<code>,pin=<pin>\n"
"	-u, --unset code=<code>,pin=<pin>\n"
"	-t, --toggle code=<code>,pin=<pin>\n"
"			A way which the <pin> should be changed after the\n"
"			<code> is received\n"
    );
    fprintf(stderr,
"	-e, --execute code=<code>,cmd=<command>,arg=<argument1>,arg=...\n"
"			A program to execute with up to %u arguments\n", MAX_CMDARGS_NUMBER
    );
    fprintf(stderr,
"	-b,		Run in background\n"
"	-p file		Specify PID file name\n"
"	-h, --help	Display this help\n"
    );
    exit(255);
}

/*
 * Review configured actions
 */
void
verbose(void)
{
    rcc_entry_t node;
    const char *action_name;

    SLIST_FOREACH(node, &rcc_head, rcc_entries) {
	action_name = long_options[node->action].name;
	printf("\t0x%X: %s ", node->code, action_name);
	if (node->action == CMD_EXECUTE) {
	    int args_count = 1;

	    printf("cmd \"%s", node->argv[0]);
	    while(
		(node->argv[args_count] != NULL) &&
		(args_count < MAX_CMDARGS_NUMBER)
	    ) {
		printf(" %s", node->argv[args_count++]);
	    }
	    printf("\"\n");
	} else {
	    printf("pin %u\n", node->pin);
	}
    }
}

/*
 * Signals handler. Prepare the programm for end
 */
static void
termination_handler(int signum)
{
    /* Free allocated memory blocks */
    while (!SLIST_EMPTY(&rcc_head)) {           /* List Deletion. */
	rcc_entry_t node = SLIST_FIRST(&rcc_head);
	SLIST_REMOVE_HEAD(&rcc_head, rcc_entries);
	free(node);
    }

    /* Close devices */
    close(dev);
    gpio_close(gpioc);

    /* Remove pidfile and exit */
    pidfile_remove(pfh);
    free(pidfile);

    exit(EXIT_SUCCESS);
}

/*
 * Daemonize the program
 */
static void
daemonize(void)
{
    pid_t otherpid;

    /* Try to create a pidfile */
    pfh = pidfile_open(pidfile, 0600, &otherpid);
    if (pfh == NULL)
    {
	if (errno == EEXIST)
	    errx(EXIT_FAILURE, "Daemon already running, pid: %d.", (int)otherpid);

	/* If we cannot create pidfile from other reasons, only warning. */
	warn("Cannot open or create pidfile");
	/*
	 * Even though pfh is NULL we can continue, as the other pidfile_*
	 * function can handle such situation by doing nothing except setting
	 * errno to EDOOFUS.
	 */
    }

    /* Try to demonize the process */
    if (daemon(0, 0) == -1)
    {
	pidfile_remove(pfh);
	errx(EXIT_FAILURE, "Cannot daemonize");
    }

    pidfile_write(pfh);
}

/*
 * Searches an entry for the code
 */
static rcc_entry_t
search_rcc_entry(const uint32_t *code)
{
    rcc_entry_t node;

    SLIST_FOREACH(node, &rcc_head, rcc_entries)
    {
	if (node->code == *code)
	    return (node);
    }

    return (NULL);
}

/*
 * Adds a new empty entry
 */
static rcc_entry_t
add_rcc_entry(rcc_entry_t curnode)
{
    rcc_entry_t newnode;

    /* calloc() for filling it by 0 */
    newnode = (rcc_entry_t)calloc(1, sizeof(*newnode));
    if (newnode == NULL)
	err(EXIT_FAILURE, "Unable to allocate memory for an action entry\n");

    if (curnode == NULL)
	SLIST_INSERT_HEAD(&rcc_head, newnode, rcc_entries);
    else if (SLIST_NEXT(curnode, rcc_entries) == NULL)
	SLIST_INSERT_AFTER(curnode, newnode, rcc_entries);
    else
	err(EXIT_FAILURE, "Unable to add an action entry\n");

    return newnode;
}

/*
 * Get gpio's suboptions
 */
static void
get_pin_subopts(int action, rcc_entry_t node, char *options) {
    char *value, *end;
    char * const subopts[] = {
#define CODE 0
	"code",
#define PIN 1
	"pin",
	NULL
    };

    node->action = action;
    while (*options) {
	switch(getsubopt(&options, subopts, &value)) {
	    case CODE:
		if (!value)
		    errx(EXIT_FAILURE, "no code");
		node->code = strtoul(value, &end, 0);
		break;
	    case PIN:
		if (!value)
		    errx(EXIT_FAILURE, "no pin number");
		node->pin = strtoul(value, &end, 0);
		break;
	    case -1:
		if (suboptarg)
		    errx(EXIT_FAILURE, "illegal sub option %s", suboptarg);
		else
		    errx(EXIT_FAILURE, "no code neither pin number");
		break;
	    default:
		break;
	}
    }
}

/*
 * Get exec's suboptions
 */
static void
get_exec_subopts(int action, rcc_entry_t node, char *options) {
    char *value, *end;
    char * const subopts[] = {
#define CODE 0
	"code",
#define CMD 1
	"cmd",
#define COMMAND 2
	"command",
#define ARG 3
	"arg",
	NULL
    };
    int args_count = 1;

    node->action = action;
    while (*options) {
	switch(getsubopt(&options, subopts, &value)) {
	    case CODE:
		if (!value)
		    errx(EXIT_FAILURE, "no code");
		node->code = strtoul(value, &end, 0);
		break;
	    case CMD:
		/* FALLTHROUGH */
	    case COMMAND:
		if (!value)
		    errx(EXIT_FAILURE, "no command string");

		node->argv[0] = value;
		break;
	    case ARG:
		if (args_count > MAX_CMDARGS_NUMBER) {
		    args_limit = true;
		    break;
		}

		if (!value)
		    errx(EXIT_FAILURE, "no argument string");

		node->argv[args_count++] = value;
		break;
	    case -1:
		if (suboptarg)
		    errx(EXIT_FAILURE, "illegal sub option %s", suboptarg);
		else
		    errx(EXIT_FAILURE, "no code neither command string");
		break;
	    default:
		break;
	}
    }
}

/*
 * Get and decode params
 */
static void
get_param(int argc, char **argv)
{
    int ch, long_index = 0;
    rcc_entry_t node;
    extern char *optarg, *suboptarg;
    char *end;

    SLIST_INIT(&rcc_head);
    node = SLIST_FIRST(&rcc_head);

    while (
	(ch = getopt_long(argc, argv, "d:g:i:s:u:t:e:p:bhv", long_options, &long_index)) != -1
    ) {
	switch (ch) {
	    case 'd':
		dev_rcrecv = optarg;
		break;
	    case 'g':
		dev_gpio = optarg;
		break;
	    case 'i':
		interval = strtoul(optarg, &end, 0);
		break;
	    case 'b':
		background = true;
		break;
	    case 'p':
		pidfile = optarg;
		break;
	    case 's':
		node = add_rcc_entry(node);
		get_pin_subopts(PIN_SET, node, optarg);
		break;
	    case 'u':
		node = add_rcc_entry(node);
		get_pin_subopts(PIN_UNSET, node, optarg);
		break;
	    case 't':
		node = add_rcc_entry(node);
		get_pin_subopts(PIN_TOGGLE, node, optarg);
		break;
	    case 'e':
		node = add_rcc_entry(node);
		get_exec_subopts(CMD_EXECUTE, node, optarg);
		break;
	    case 'v':
		be_verbose = true;
		break;
	    case 'h':
		/* FALLTHROUGH */
	    default:
		usage();
		exit(EXIT_SUCCESS);
	}
    }
    if (be_verbose) {
	verbose();
    };
    if (args_limit) {
	warnx("Only %u command arguments were configured at compile time", MAX_CMDARGS_NUMBER);
    };
}

/*
 * Do a pin action defined in the node
 */ 
void
do_gpio_action(rcc_entry_t node) {
    const char *action_name = long_options[node->action].name;
    /* Set the pin to output mode and do an action */
    gpio_pin_output(gpioc, node->pin);
    switch (node->action) {
	case PIN_SET:
	    gpio_pin_high(gpioc, node->pin);
	    break;
	case PIN_UNSET:
	    gpio_pin_low(gpioc, node->pin);
	    break;
	case PIN_TOGGLE:
	    gpio_pin_toggle(gpioc, node->pin);
	    break;
	default:
	    syslog(LOG_ERR, "Cannot %s pin %u on code 0x%X: action %i is not known",
		    action_name, node->pin, node->code, node->action);
	    return;
    }
    syslog(LOG_INFO, "On 0x%X %s pin %u",
	    node->code, action_name, node->pin);
}

/*
 * Exec a command deined int the node
 */
void
do_exec_action(rcc_entry_t node) {
    const char *action_name = long_options[node->action].name;
    pid_t otherpid = fork();
    if (otherpid == 0) {
	/* Execute a new process defined in argv[] */
	(void)execv(node->argv[0], node->argv);
	/* Oops! This should not have happened */
	syslog(LOG_ERR, "Cannot %s \"%s\" on code 0x%X code: %m",
		action_name, node->argv[0], node->code);
    } else if (otherpid < 0) {
	/* fork() has been unsuccesful */
	syslog(LOG_ERR, "Cannot %s \"%s\" on code 0x%X code: %m",
		action_name, node->argv[0], node->code);
    } else {
	/* execv() done OK */
//	pid_t status = wait(&otherpid);
	syslog(LOG_ERR, "On 0x%X %s \"%s\"",
		node->code, action_name, node->argv[0]);
    }
}

int
main(int argc, char **argv)
{
    rcc_entry_t node;

    struct timespec timeout;
    const size_t waitms = 1000;
    int64_t last_time = 0;
    uint32_t last_code = 0;

    struct kevent event;    /* Event monitored */
    struct kevent tevent;   /* Event triggered */
    int kq, ret;
    struct rcrecv_code rcc;

    get_param(argc, argv);

    /* Set a timeout by 'waitms' value */
    timeout.tv_sec = waitms / 1000;
    timeout.tv_nsec = (waitms % 1000) * 1000 * 1000;

    /* Open GPIO controller */
    gpioc = gpio_open_device(dev_gpio);
    //gpio_pin_output(handle, 16);
    if (gpioc == GPIO_INVALID_HANDLE)
	errx(EXIT_FAILURE, "Failed to open '%s'", dev_gpio);

    /* Open RCRecv device */
    dev = open(dev_rcrecv, O_RDONLY);
    if (dev < 0)
	errx(EXIT_FAILURE, "Failed to open '%s'", dev_rcrecv);

    /* Create kqueue. */
    kq = kqueue();
    if (kq == -1)
	err(EXIT_FAILURE, "kqueue() failed");

    /* Initialize kevent structure. */
    EV_SET(&event, dev, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, NULL);
    /* Attach event to the kqueue. */
    ret = kevent(kq, &event, 1, NULL, 0, NULL);
    if (ret == -1)
	err(EXIT_FAILURE, "kevent register");
    if (event.flags & EV_ERROR)
	errx(EXIT_FAILURE, "Event error: %s", strerror(event.data));

    /* If there are dirty kevents read and drop irrelevant data */
    ret = kevent(kq, NULL, 0, &tevent, 1, &timeout);
    if (ret == -1) {
	err(EXIT_FAILURE, "kevent wait");
    }
    else if (ret > 0) {
	ioctl(dev, RCRECV_READ_CODE_INFO, &rcc);
    }

    /* Unbinds from terminal if '-b' */
    if (background) {
	daemonize();

	/* Create kqueue for child */
	kq = kqueue();
	if (kq == -1)
	    err(EXIT_FAILURE, "kqueue() failed");

	/* Initialize kevent structure once more */
	EV_SET(&event, dev, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, NULL);
	/* and  once more attach event to the kqueue. */
	ret = kevent(kq, &event, 1, NULL, 0, NULL);
	if (ret == -1)
	    err(EXIT_FAILURE, "kevent register");
	if (event.flags & EV_ERROR)
	    errx(EXIT_FAILURE, "Event error: %s", strerror(event.data));
    }

    /* Intercept signals to our function */
    if (signal (SIGINT, termination_handler) == SIG_IGN)
	signal (SIGINT, SIG_IGN);
    if (signal (SIGTERM, termination_handler) == SIG_IGN)
	signal (SIGTERM, SIG_IGN);

    for (;;) {
	/* Sleep until a code received */
	ret = kevent(kq, NULL, 0, &tevent, 1, &timeout);
	if (ret == -1) {
	    err(EXIT_FAILURE, "kevent wait");
	}
	else if (ret > 0) {
	    /* Get a code from remote control
	       and search a node for it */
	    ioctl(dev, RCRECV_READ_CODE_INFO, &rcc);
	    node = search_rcc_entry(&rcc.value);
	    /* No actions if the same code is repeated too fast */
	    if (node != NULL &&
	       (last_code != rcc.value ||
		llabs(last_time - rcc.last_time) > (interval * 1000)))
	    {
		/* Do the pin action as set in the node */
		switch(node->action) {
		    case PIN_SET:
			/* FALLTHROUGH */
		    case PIN_UNSET:
			/* FALLTHROUGH */
		    case PIN_TOGGLE:
			do_gpio_action(node);
			break;
		    case CMD_EXECUTE:
			do_exec_action(node);
			break;
		    default:
			break;
		}
		last_time = rcc.last_time;
		last_code = rcc.value;
	    }
	}
    }
}
