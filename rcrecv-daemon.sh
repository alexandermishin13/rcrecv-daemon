#!/bin/sh
#
# $FreeBSD: 2021-05-21 21:24500Z mishin $
#
# PROVIDE: rcrecvn
# REQUIRE: DAEMON
# KEYWORD: nojail shutdown
#
# Add the following lines to /etc/rc.conf to enable rcrecvn:
#
# rcrecv_enable (bool):	Set to "NO"  by default.
#				Set to "YES" to enable bh1750_daemon.
# rcrecv_daemon_codes (str):	Set to "" by default.
#				Define your profiles here.
# rcrecv_daemon_flags (str):	Set to "" by default.
#				Extra flags passed to start command.
#
# For a profile based configuration use variables like this:
#
# rcrecv_daemon_codes="XXX YYY"
# rcrecv_XXX_pin=<pin1>
# rcrecv_XXX_state=[s|u|t] # set|unset|toggle. Default is "t"
# rcrecv_YYY_pin=<pin2>
# rcrecv_YYY_state=[s|u|t]

. /etc/rc.subr

name=rcrecv_daemon
rcvar=rcrecv_daemon_enable

load_rc_config $name

: ${rcrecv_daemon_enable:="NO"}
: ${rcrecv_daemon_flags:="-b"}
: ${rcrecv_daemon_user:="root"}
: ${rcrecv_daemon_group:="wheel"}

pidfile=${rcrecv_daemon_pidfile:-"/var/run/rcrecv-daemon/pid"}
rcrecv_daemon_bin="/usr/local/sbin/rcrecv-daemon"

codes_flags=""
for code in $rcrecv_daemon_codes; do
	eval code_pin="\${rcrecv_${code}_pin}"
	eval code_command="\${rcrecv_${code}_command}"
	if [ -n "$code_pin" ]; then
		eval code_state="\${rcrecv_${code}_state:-t}"
		codes_flags="${codes_flags} -${code_state} code=${code},pin=${code_pin}"
	elif [ -n "$code_command" ]; then
		opt_name="cmd"
		codes_flags="${codes_flags} -e code=${code}"
		for cmd_arg in $code_command; do
			codes_flags="${codes_flags},${opt_name}=${cmd_arg}"
			opt_name="arg"
		done
	fi
done

command=${rcrecv_daemon_bin}
command_args="${codes_flags} ${rcrecv_daemon_flags} -p ${pidfile}"
echo ${command_args}

start_precmd="${name}_precmd"
rcrecv_daemon_precmd()
{
	install -d -o ${rcrecv_daemon_user} -g ${rcrecv_daemon_group} /var/run/rcrecv-daemon
}

run_rc_command "$@"
