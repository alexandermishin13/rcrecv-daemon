.\"-
.\"Copyright (c) 2021, Alexander Mishin
.\"All rights reserved.
.\"
.\"Redistribution and use in source and binary forms, with or without
.\"modification, are permitted provided that the following conditions are met:
.\"
.\"* Redistributions of source code must retain the above copyright notice, this
.\"  list of conditions and the following disclaimer.
.\"
.\"* Redistributions in binary form must reproduce the above copyright notice,
.\"  this list of conditions and the following disclaimer in the documentation
.\"  and/or other materials provided with the distribution.
.\"
.\"THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
.\"AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\"IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
.\"DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
.\"FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\"DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
.\"SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
.\"CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
.\"OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
.\"OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.Dd May 21, 2021
.Dt RCRECV-DAEMON 8
.Os
.Sh NAME
.Nm rcrecv-daemon
.Nd GPIO control daemon for
.Xr rcrecv 4
kernel driver.
.Sh SYNOPSIS
.Nm
.Op Fl b
.Op Fl d Ar device
.Op Fl g Ar device
.Op Fl i Ar ms
.Oo
.Fl ( s Ns | Ns Fl u Ns | Ns Fl t Ns | Ns Fl e )
.Ar suboptions
.Oc
.Op Fl h
.Sh OPTIONS
The
.Nm
supports the following options:
.Bl -tag -width indent
.It Fl b
Daemonize the
.Nm
after running;
.It Fl d , Ic --device Ar device
Specify a
.Xr rcrecv 4
character device. The compiled-in default is
.Pa /dev/rcrecv Ns ;
.It Fl g , Ic --gpio Ar device
Specify a gpio controller. The compiled-in default is
.Pa /dev/gpioc0 Ns ;
.It Fl i , Ic --interval Ar ms
Specify a minimal interval for ignoring the same codes repeated too fast. Default value is 1000ms;
.It Fl s , Ic --set Ar code Ns = Ns Ar value , Ns Ar pin Ns = Ns Ar number
.It Fl u , Ic --unset Ar code Ns = Ns Ar value , Ns Ar pin Ns = Ns Ar number
.It Fl t , Ic --toggle Ar code Ns = Ns Ar value , Ns Ar pin Ns = Ns Ar number
Set, unset or toggle the gpio
.Ar pin
when the
.Ar code
is received.
.Ar Value
and
.Ar number
must be integer constants, with a leading 0x indicating a hexadecimal value and
a leading 0 indicating an octal value. Options
.Fl s , Fl u
and
.Fl t
can be used multiple times in any order;
.It Fl e , Ic --execute Ar code Ns = Ns Ar value , Ns Ar cmd Ns = Ns Ar command , Ns Ar arg Ns = Ns Ar argument
Execute a
.Ar command
with a number of
.Ar argument Ns s when the
.Ar code
is received. Neither the command nor argument string can contain spaces or commas, but the
.Ar arg
suboption can be used multiple times to describe multiple arguments. The command name must have a
full path.
.It Fl h , Ic --help
Brief help.
.Sh DESCRIPTION
The kernel module
.Xr rcrecv 4
must be loaded for the
.Nm
to work.
.br
When the daemon is started it interprets
.Ic --set , --unset
and
.Ic --toggle
parameters to make a list of requested states of pins for each code to receive.
Then the daemon waits a
.Xr kevent 2
from
.Xr rcrecv 4
which means that there is a code for it. The daemon gets the code, seek a pin
defined for it, configures the pin for output and changes state of that pin
the way have been defined by a corresponding parameter.
.Sh EXAMPLES
An example of a
.Xr service 8
configuration file looks like this:
.Bd -literal -offset indent
rcrecv_daemon_enable="YES"
#rcrecv_daemon_user="gpio"
rcrecv_daemon_codes="0x952e91 0x952e92 0x952e94 0x952e98"
rcrecv_0x952e91_pin=12
rcrecv_0x952e91_state="t"
rcrecv_0x952e92_pin=11
rcrecv_0x952e92_state="s"
rcrecv_0x952e94_pin=11
rcrecv_0x952e94_state="u"
rcrecv_0x952e98_cmd="/usr/local/bin/curl http://tasmota.local/cm?cmnd=POWER3%20TOGGLE"
rcrecv_0x952e98_state="e"
.Ed

This will make the
.Xr service 8
script to run the daemon with following options:
.Bd -offset indent 
$ rcrecv-daemon -b -t code=0x952e91,pin=12 -s code=0x952e92,pin=11 -u code=0x952e94,pin=11 -e code=0x952e98,cmd=/usr/local/bin/curl,arg=http://tasmota.local/cm?cmnd=POWER3%20TOGGLE
.Ed

The service can be started as an unprivileged user. To do this you need to
uncomment a
.Ql rcrecv_daemon_user
option.

You also need to take care to set the necessary permissions on the
device file
.Ql /dev/rcrecv
for this user. For example, to add read access for the user
.Ql gpio
simply add the following lines to the bottom of the
.Xr devfs.conf 5 file and restart the Xr devfs 8 service:
.Bd -literal -offset indent
own  rcrecv gpio
perm rcrecv 0400
.Ed

Note: If You want to make a
.Xr service 8 configuration file in the
.Pa rc.conf.d/
directory, the file must be named
.Ql rcrecv_daemon
(with an underscore, not a hyphen).

.Sh FILES
/usr/local/etc/rc.d/rcrecv-daemon
.Sh SEE ALSO
.Xr rcrecv 4 ,
.Xr devfs.conf 5 ,
.Xr devfs 8 ,
.Xr gpioctl 8
.Sh AUTHORS
The
.Nm
was written by
.An Alexander Mishin Aq Mt mishin@mh.net.ru .
